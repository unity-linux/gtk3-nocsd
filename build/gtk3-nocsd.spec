%define debug_package %{nil}
%define snapshot_date   20160617
%define snapshot_hash   master

Name: gtk3-nocsd
Version: 3
Release: 3.%{snapshot_date}git%{snapshot_hash}%{?dist}
Summary: Disables Gtk+ 3 client side decorations (such as Gnome header bars)
License: LGPLv2+
URL: https://github.com/PCMan/gtk3-nocsd
Source0: https://github.com/PCMan/gtk3-nocsd/archive/%{snapshot_hash}.tar.gz
Source1: 70-gtk3-nocsd.sh
Patch: gtk3-nocsd-manpage.patch
Requires: %{name}-lib%{?_isa} = %{version}
%ifarch x86_64
Requires: %{name}-lib(x86-32)
%endif
BuildRequires: pkgconfig, gtk3-devel, gtk+-devel, gobject-introspection-devel

%description
gtk3-nocsd is a small module that disables Gtk+ 3 client side decorations.

Since Gtk+ 3.10, many GNOME applications have begun using client-side header
bars, which combine traditional window manager-provided title bars with
client-side menu and tool bars. When used with gnome-shell, header bar
applications use less screen space for window controls, and can directly update
their own controls. The GNOME project has made project-wide adoption of header
bars an official goal.

However, header bar applications can have integration problems with non-GNOME
desktop environments: blocking non-gnome shell window manager features like
window shading, and presenting an inconsistent UI / look. Unfortunately, Gtk+
3.20 does not provide an option to turn client side decorations off in these
cases. This LD_PRELOADable library (partially) turns them off, reenabling the
window manager-provided title bar and window border.

%package lib
Summary: library files for gtk3-nocsd
Provides: %{name}-lib%{?_isa} = %{version}

%description lib
This is a library file for gtk3-nocsd, a small module that disables 
Gtk+ 3 client side decorations. This subpackage is needed for
multi-architecture setups

%prep
%setup -q -n %{name}-master
%patch -p1

%build
env CFLAGS="%{optflags}" make

%define xinitdir %{_sysconfdir}/X11/xinit/xinitrc.d

%install
env prefix=%{_usr} libdir=%{_libdir} DESTDIR="%{buildroot}" make install
# b/c gtk3-nocsd installs its shared lib w/ mode 644, confusing find-debuginfo.sh
find "%{buildroot}/%{_libdir}" -type f ! -perm /0111 |file -N -f - |sed -n -e 's/^\(.*\):[ 	]*.*ELF.*, not stripped.*/\1/p' |xargs --no-run-if-empty chmod a+x
gzip -9 %{buildroot}%{_mandir}/man1/%{name}.1
install -p -m 0644 -D -t %{buildroot}%{xinitdir} %SOURCE1

%check
make check

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc COPYING gedit-with-csd.png gedit-without-csd.png README.md TODO
%attr(755,root,root) %{_bindir}/%{name}
%attr(644,root,root) %{_datadir}/bash-completion/completions/%{name}
%attr(644,root,root) %{_mandir}/man1/%{name}.1*
%attr(755,root,root) %{xinitdir}/*

%files lib
%attr(755,root,root) %{_libdir}/lib%{name}.so*

%changelog
* Wed Feb 20 2019 JMiahMan <JMiahMan@unity-linux.org> - 3-2.20160617git82ff5a0d
- Have X86_64 Require i686 lib to prevent errors. 

* Fri Jan 25 2019 JMiahMan <JMiahMan@unity-linux.org> - 3-1.20160617git82ff5a0d
- Initial build for Unity-Linux

* Thu Jul 27 2017 Honore Doktorr <hdfssk@gmail.com> - 3-1.20160617git82ff5a0d
- Initial packaging for Fedora 26
