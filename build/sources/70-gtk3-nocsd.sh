#!/bin/sh

# gtk3-nocsd configuration

# Whether to disable CSD on non-GNOME desktops
#    0: disable CSD
#    1: enable CSD
# If this setting is left unset, it will default to 0 (CSD disabled)
# unless GTK3_NOCSD_IGNORE is set to 1, in which case it will remain
# unset. (With gtk defaulting to showing CSDs.)

#export GTK_CSD=0

# Whether to not preload libgtk3-nocsd.so.0 even if GTK_CSD=0
# If this setting is set to 1, LD_PRELOAD and GTK_CSD will not be
# touched upon session startup.

#export GTK3_NOCSD_IGNORE=0

# Check if /usr/bin/gtk3-nocsd is executable, to make sure that the
# case where gtk3-nocsd is removed but not yet purged is also
# handled properly.
if [ -x /usr/bin/gtk3-nocsd ] && ( [ -z "$GTK3_NOCSD_IGNORE" ] || [ x"$GTK3_NOCSD_IGNORE"x = x"0"x ] ) ; then
    BASESTARTUP=${STARTUP%% *}
    BASESTARTUP=${BASESTARTUP##*/}
    if [ "$BASESTARTUP" = x-session-manager ]; then
        BASESTARTUP=$(basename $(readlink /etc/alternatives/x-session-manager))
    fi
    case "$BASESTARTUP" in
      gnome-session*)
          # This is GNOME, make sure GTK_CSD is not set to 0
          if [ x"$GTK_CSD"x = x"0"x ] ; then
              unset GTK_CSD
          fi
          ;;
      *)
          # not GNOME, and the user didn't specify GTK_CSD
          # themselves, so set it to 0
          if [ -z "$GTK_CSD" ] ; then
              GTK_CSD=0
          fi
          export GTK_CSD
          if [ x"$GTK_CSD"x = x"0"x ] ; then
              export LD_PRELOAD="libgtk3-nocsd.so.0${LD_PRELOAD:+:$LD_PRELOAD}"
          fi
          ;;
    esac
fi


# ★ ↓ is original 70, ↑ was 51, so ↓ still needed?
# ★ maybe above set at 51 to support particular programs — ¿ author?

# Some other startup programs may drop LD_PRELOAD as environment
# variable (ssh-agent is known for this), so make sure it's set
# explicitly again AFTER ssh-agent has been called.
if [ -n "$LD_PRELOAD" ] ; then
    STARTUP="env LD_PRELOAD=$LD_PRELOAD $STARTUP"
fi
